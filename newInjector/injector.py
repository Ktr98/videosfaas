import random
import numpy as np
import pandas as pd
class ChargeInjector():
    
    def __init__(self, func_name, average, duration):
        self.func_name = func_name
        self.average = average
        self.duration = duration
        self.n_invs = int(duration/average)
        
    def getDuration(self):
        return self.duration

    def sampler(self):
        l = np.random.exponential(scale=self.average, size=self.n_invs)
        df = pd.DataFrame(l, columns=['inter-arrival times'])
        df.to_csv(self.func_name+"-"+str(self.duration)+"-"+str(self.average)+".csv", index=False)
        return  l
