import os
import threading
import time

class Launch(threading.Thread):
    def __init__(self, function):
        threading.Thread.__init__(self)
        self.function = function
    def run(self):
        command = "python3 activation-id-generator.py "+function+"-1800-60.csv "+function+"-params.csv outputs"
        os.system(command)

functions = ["wand_denoise", "wand_blur", "wand_edge", "wand_resize", "wand_rotate", "wand_sepia"]

for function in functions:
    launch = Launch(function)
    launch.start()
    time.sleep(1)
