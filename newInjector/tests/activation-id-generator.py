import sys, os
import pandas as pd
import threading
import time

class Invoke(threading.Thread):
    def __init__(self, function_name, params_names, params_values):
        threading.Thread.__init__(self)
        self.function_name = function_name
        self.params_names = params_names
        self.params_values = params_values
    def run(self):
        command = "/mnt/SlimFaaS/openwhisk/bin/wsk -i action invoke "+self.function_name
        for (name,value) in zip(self.params_names, self.params_values):
            command = command+" -p "+name+" "+value
        command = command+" >> activationIds-"+self.function_name+".csv"
        print(command)
        os.system(command)

iats_file = sys.argv[1]
params_file = sys.argv[2]

iats = pd.read_csv(iats_file)
params = pd.read_csv(params_file)
function_name = iats_file.split("-")[0]
params_names = list((pd.read_csv(params_file)).keys())

aidfile = open("activationIds-"+function_name+".csv", "w")
aidfile.write("aids\n")
aidfile.close()

for i in range(len(iats)):
    params_values=[params[key][i] for key in params_names]
    invk = Invoke(function_name, params_names, params_values)
    invk.start()
    time.sleep(float(iats["inter-arrival times"][i]))
    
params_values=[params[key][len(iats)] for key in params_names]
invk = Invoke(function_name, params_names, params_values)
invk.start()

aid = pd.read_csv("activationIds-"+function_name+".csv")
for i in range(len(aid)):
    aid["aids"][i] = aid["aids"][i].split(" ")[-1]
aid.to_csv("activationIds-"+function_name+".csv", index=False)
    




