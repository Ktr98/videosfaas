import sys
import swiftclient

container=sys.argv[4]
auth=sys.argv[3]

user = sys.argv[1]
key = sys.argv[2]
conn = swiftclient.Connection(
        user=user,
        key=key,
        authurl=auth,
)

objs=[]
for data in conn.get_container(container)[1]:
        conn.delete_object(container, data['name'])
