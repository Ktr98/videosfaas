import sys

json = open(sys.argv[1], "r")
csv = open(sys.argv[2], "w")
n_line = 0
for line in json:
    n_line+=1
    line = line.replace("{", "")
    line = line.replace("}", "")
    line = line.replace("\n", "")
    vect = line.split(",")
    n_attr = len(vect)
    if(n_line==1):
        newline = ""
        for i in range(n_attr-1):
            if i != n_attr-2 : newline = newline + ((vect[i].split(":")[0]).strip()) + ";"
            else : newline = newline + ((vect[i].split(":")[0]).strip())
        csv.write(newline+"\n")
        newline = ""
        for i in range(n_attr-1):
            if i != n_attr-2 : newline = newline + ((vect[i].split(":")[1]).strip()) + ";"
            else : newline = newline + ((vect[i].split(":")[1]).strip())
        csv.write(newline+"\n")
    else:
        newline = ""
        for i in range(n_attr-1):
            if i != n_attr-2 : newline = newline + ((vect[i].split(":")[1]).strip()) + ";"
            else : newline = newline + ((vect[i].split(":")[1]).strip())
        csv.write(newline+"\n")
json.close()
csv.close()