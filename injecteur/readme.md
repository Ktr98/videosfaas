# Waiting time generation

What is interesting is each file named like this : `function_name-inv_avg.csv`

* `function_name` : refers to the name of the videos processing function_name
* `inv_avg` : refers to the average of the wating time to the next invacation

## The way to generate those files

### Requirements
* Have, apache openwhisk installed, and well configured in a VM
* Have swiftclient python package installed
* Have a swift VM well configured

### Steps

The differents steps to generate thoses files are :

* **step 1** : Downloading videos files (That a step you can manage your self)
* **step 2** : Uploading all those videos on a remote storage (openstack swift for example). For this step we use the file `put-in-swift.py`, which consists of Uploading on a swift VM a specific file.
    We use it like this  : 
    ```sh
    python put-in-swift.py $USER $KEY $AUTHURL $CONTAINER $FILE_PATH
    ```
    or to put all the files of a folder on a remote storage:
    ```sh
    python vid-to-swift.py $USER $KEY $AUTHURL $CONTAINER $FOLDER_PATH
    ``` 
* **step 3** : Writing the function which will process on those videos (You are supposed to manage this step your self also)
* **step 4** : Running the functions written with this command : 

    ```sh
    python run-all-actions.py $USER $KEY $AUTHURL $FUNCTION_NAME1 $FUNCTION_NAME2 ... $FUNCTION_NAMEn $IN_CONTAINER $OUT_CONTAINER
    ```
* **step 5** : The previous step generate the json files of te functions executed like this `res_function_name.json`
    We need to have those files in csv, that's the goal of this step : to do that, we use the following commad : 
    ```sh
    python those-json2csv.py $JSON_FILE1 $JSON_FILE2 ... $JSON_FILEn
    ```
* **step 6** : The previous step generate the json files of te functions executed like this `csv_function_name.csv`, In this step, we transforme it to the file expected, by using this command : 
    ```sh
    python gen-time-inv.py $CSV_FILE
    ```
    The output has the following shape : `function_name-inv_avg.csv`