import os
import swiftclient
import sys

auth=sys.argv[3]
container = sys.argv[4]
files = os.listdir(sys.argv[5])

user = sys.argv[1]
key = sys.argv[2]
conn = swiftclient.Connection(
        user=user,
        key=key,
        authurl=auth,
)

for obj in files:
    with open(obj, 'rb') as local:
        conn.put_object(container, obj, contents=local,)