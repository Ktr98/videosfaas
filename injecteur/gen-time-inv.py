import pandas as pd
import sys

data = pd.read_csv(sys.argv[1], sep=";")

times = [0]

for i in range(1, len(data)):
    times.append(float(data["launching_time"][i])-float(data["launching_time"][i-1]))

data["waiting"] = times

data = data["waiting"]

data.to_csv((sys.argv[1]).split(".")[0]+"-"+str(data.mean())+"s.csv", index=False)