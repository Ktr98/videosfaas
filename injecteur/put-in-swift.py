#!/usr/bin/env python3
import os
import time
import swiftclient
import sys

conn = swiftclient.Connection(
    user=sys.argv[1],
    key=sys.argv[2],
    authurl=sys.argv[3],
)

obj = sys.argv[5]
container = sys.argv[4]
with open(obj, 'rb') as local:
    conn.put_object(container, obj, contents=local,)