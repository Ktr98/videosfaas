package com.company;

import weka.classifiers.Classifier;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.Range;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/*
 * This class represents the module which is in charge of :
 * - importing data
 * - formatting data so that its can be processed by weka
 * - splitting the dataset in both training and testing sets (and set them available)
 * - build a model of predictions
 * - export the model of prediction in a specific file (a .model file)
 */

public class ModelTrainer {

    private String datafile = ""; // The path of the source of data about runs
    private int min = 0; // The minimum of memory expected
    private int max = 2000000000; // The maximum of memory expected
    private int n = 128; // The number of clusters
    private Classifier memory_model; // The model built for memory predictions
    private Classifier use_cache_model; // The model built for using cache predictions
    private static float rate = (float)0.5; // the threshold to decide the benefits of using the cache
    private Instances dataset;
    private static Instances training_set;
    private static Instances testing_set;
    private Instances dm;
    private Instances dcu;

    // Constructor
    public ModelTrainer(String datafile, int min, int max, int n, float rate) {
        this.datafile = datafile;
        this.min = min;
        this.max = max;
        this.n = n;
        this.rate = rate;
    }

    // Getters and Setters

    public String getDatafile() {
        return datafile;
    }

    public void setDatafile(String datafile) {
        this.datafile = datafile;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public Instances getTraining_set() {
        return training_set;
    }

    public Instances getTesting_set() {
        return testing_set;
    }

    public Instances getDataset() {
        return dataset;
    }

    public void setDataset(Instances dataset) {
        this.dataset = dataset;
    }

    public Classifier getMemory_model() {
        return memory_model;
    }

    public void setMemory_model(Classifier memory_model) {
        this.memory_model = memory_model;
    }

    public Classifier getUse_cache_model() {
        return use_cache_model;
    }

    public void setUse_cache_model(Classifier use_cache_model) {
        this.use_cache_model = use_cache_model;
    }

    public Instances getDm() {
        return dm;
    }

    public void setDm(Instances dm) {
        this.dm = dm;
    }

    public Instances getDcu() {
        return dcu;
    }

    public void setDcu(Instances dcu) {
        this.dcu = dcu;
    }

    // The methods

    // Importing data and formatting them respect to weka
    public void import_data() throws Exception {
        // importing an arff file containing data
        File f = new File(this.datafile);
        ConverterUtils.DataSource source = new ConverterUtils.DataSource(f.getAbsolutePath());
        Instances data = source.getDataSet();
        this.setDataset(data);
    }

    // Splitting the dataset into training and testing sets
    public void split_dataset(){
        Instances data_set = this.getDataset();
        data_set.setClassIndex(data_set.numAttributes()-2);
        data_set.setClassIndex(data_set.numAttributes()-1);
        // Classes of using cache
        List val = new ArrayList();
        for(int i=0; i<2; i++){
            val.add(""+i+"");
        }

        // Classes of memory
        List values = new ArrayList();
        for(int i=1; i<=this.n; i++){
            values.add(""+i+"");
        }

        // Adding Those classes' attributes
        int n_att = data_set.numAttributes();
        data_set.insertAttributeAt(new Attribute("Cluster", values), n_att);
        data_set.insertAttributeAt(new Attribute("InCache", val), n_att+1);
        for (int i = 0; i < data_set.numInstances(); i++) {
            float video_size = (float) data_set.instance(i).value(4);
            float nb = (float)(video_size-this.min)*this.n/(this.max-this.min);
            int e_nb = (int)nb;
            int cluster = 0;
            if (nb-e_nb>0){
                cluster = e_nb + 1;
            }
            else{
                cluster = e_nb;
            }

            float in = (float) data_set.instance(i).value(1);
            float out = (float) data_set.instance(i).value(3);
            float compute = (float) data_set.instance(i).value(2);
            float ratio = (in+out)/(in+compute+out);
            int group = 0;
            if (ratio > this.rate){
                group = 1;
            }
            data_set.instance(i).setValue(data_set.numAttributes() - 2, ""+cluster+"");
            data_set.instance(i).setValue(data_set.numAttributes() - 1, ""+group+"");
        }


        // Splitting effectively the dataset
        data_set.randomize(new java.util.Random());	// randomize instance order before splitting dataset
        this.training_set = data_set.trainCV(5, 0);
        this.testing_set = data_set.testCV(2, 0);
    }

    // Build a model and save it
    public void build_and_save_model() throws Exception {

        Instances data = this.getDataset();
        Instances training = this.getTraining_set();
        Instances testing = this.getTesting_set();

        // Memory's predictions model
        Remove rm_for_memory = new Remove();
        rm_for_memory.setAttributeIndices(Range.indicesToRangeList(new int[]{1,2,3,4,6}));
        rm_for_memory.setInputFormat(training);
        Instances data_memory = Filter.useFilter(data, rm_for_memory);
        this.setDm(data_memory);
        Instances training_memory = Filter.useFilter(training, rm_for_memory);
        Instances testing_memory = Filter.useFilter(testing, rm_for_memory);
        training_memory.setClassIndex(training_memory.numAttributes()-1);
        testing_memory.setClassIndex(testing_memory.numAttributes()-1);
        Classifier cm = new J48();

        cm.buildClassifier(training_memory);
        this.setMemory_model(cm);

        // Cache usage's predictions model
        Remove rm_for_cache_usage = new Remove();
        rm_for_cache_usage.setAttributeIndices(Range.indicesToRangeList(new int[]{1,2,3,4,5}));
        rm_for_cache_usage.setInputFormat(training);
        Instances data_cache_usage = Filter.useFilter(data, rm_for_cache_usage);
        this.setDcu(data_cache_usage);
        Instances training_cache_usage = Filter.useFilter(training, rm_for_cache_usage);
        Instances testing_cache_usage = Filter.useFilter(testing, rm_for_cache_usage);
        training_cache_usage.setClassIndex(training_cache_usage.numAttributes()-1);
        testing_cache_usage.setClassIndex(testing_cache_usage.numAttributes()-1);
        Classifier ccu = new J48();
        ccu.buildClassifier(training_cache_usage);
        this.setUse_cache_model(ccu);

        // Saving models
        weka.core.SerializationHelper.write("model_memory_"+this.datafile.split("\\.")[0]+".model", cm);
        weka.core.SerializationHelper.write("model_use_cache_"+this.datafile.split("\\.")[0]+".model", ccu);
    }
}
