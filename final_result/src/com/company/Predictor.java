package com.company;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Predictor {

    private  String modelfile = "";
    private int n=128;
    private int min=0;
    private int max=2000000000;

    // Constructor

    public Predictor(String modelfile, int min, int max, int n) {
        this.modelfile = modelfile;
        this.n = n;
        this.min = min;
        this.max = max;
    }

    // Getters and Setters
    public String getModelfile() {
        return modelfile;
    }

    public void setModelfile(String modelfile) {
        this.modelfile = modelfile;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    // The methods

    // This function returns a cluster between 1 and 128 if the model is specified and 0 else
    public float predict_cluster(float video_size) throws Exception {

        File model_file;
        String path = this.modelfile;
        model_file = new File(path);

        //NEW ADDED
        int num = this.n;
        Attribute inputsize = new Attribute("inputsizes");
        List<String> clusters = new ArrayList<String>() {

            {
                for (int i=1; i<=num; i++){
                    add(""+i);
                }
            }
        };
        ArrayList<Attribute> attributeList = new ArrayList<Attribute>(2) {
            {
                add(inputsize);
                Attribute attributeClass = new Attribute("clusters", clusters);
                add(attributeClass);
            }
        };
        Instances data = new Instances("data",
                attributeList, 1);
        // last feature is target variable
        data.setClassIndex(data.numAttributes() - 1);

        double[] ninst = new double[data.numAttributes()];

        ninst[0] = video_size;
        ninst[1] = 1;
        data.add(new DenseInstance(1.0, ninst));
        if (model_file.exists()) {
            try {
                Classifier cm = (Classifier) weka.core.SerializationHelper.read(path);
                //System.out.print(data);
                double cluster = cm.classifyInstance(data.lastInstance());
                return (float) cluster+1;
            }catch (Exception e){
                e.printStackTrace();
            }
            return (float) 0;
        } else {
            System.out.println("You can't make prediction, there is no model file");
            return 0;
        }
    }

    public float predict_memory(float cluster){
        return (float) (this.min+(cluster*(this.max-this.min)/this.n));
    }

    // This function returns 1 if the cache should be used, 0 when not and "NaN if there is no model specified"
    public Float predict_cache_usage(float video_size) throws Exception {

        File model_file;
        String path = this.modelfile;
        model_file = new File(path);

        //NEW ADDED
        Attribute inputsize = new Attribute("inputsize");
        List<String> clusters = new ArrayList<String>() {
            {
                for (int i=0; i<2; i++){
                    add(""+i);
                }
            }
        };
        ArrayList<Attribute> attributeList = new ArrayList<Attribute>(2) {
            {
                add(inputsize);
                Attribute attributeClass = new Attribute("@@class@@", clusters);
                add(attributeClass);
            }
        };
        Instances data = new Instances("data",
                attributeList, 1);
        // last feature is target variable
        data.setClassIndex(data.numAttributes() - 1);

        double[] ninst = new double[data.numAttributes()];

        ninst[0] = video_size;
        ninst[1] = 0;

        data.add(new DenseInstance(1.0, ninst));

        if (model_file.exists()) {
            Classifier ccu = (Classifier) weka.core.SerializationHelper.read(path);
            double Group = ccu.classifyInstance(data.lastInstance());
            return (float)Group;
        } else {
            System.out.println("You can't make prediction, there is no model file");
            return (float)(-1);
        }
    }
}
