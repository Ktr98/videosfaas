package com.company;


public class Main {

	public static void main(String[] args) throws Exception {

		/*
		 * Process to use the ML module
		 */

		int min = 0;
		int max = 2000000000;
		float rate = (float)0.5;


		String datafile = "fonction2.arff";
		int n = 128;

		ModelTrainer mt = new ModelTrainer(datafile, min, max, n, rate);
		mt.import_data();
		mt.split_dataset();
		mt.build_and_save_model();

		String modelfile = "model_memory_fonction2.model";
		Predictor p_memory = new Predictor(modelfile, min, max, n);
		System.out.println(p_memory.predict_cluster(2506752));
		System.out.println(p_memory.predict_memory(p_memory.predict_cluster(2506752)));
		
		modelfile = "model_use_cache_fonction2.model";
		Predictor pcu = new Predictor(modelfile, min, max, n);
		System.out.print(pcu.predict_cache_usage(2506752));



	}

}

