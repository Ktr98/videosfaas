# ML module process 

This folder presents the way to usage the ML module for videos predictions

## Description of the project

This a java project, which works with 3 main files located in `src/com/company`: 
* `Main.java` : is the main class, the one we launch;
* `ModelTrainer.java`: This class, is used to :
    * Import data comming from a .arff file and format them so that weka can process on;
    * Split the dataset formated into both training and testing sets;
    * Build a ML model and save it into a .model file;
* `Predictor.java` : is the class, in charge of predicting both the memory used by a specific function and the benifit of cache storage.

There are also other usefull files : 
* `fonction2.arff, fonction3.arff, fonction4.arff` : are data file containing runs about function which updates the volume of a video, resizes a video and change the starting of video respectively;
* `result_videos.csv` : contains the result of clusters predictions, it gives both the accuracies of Exact clusters prediction and (Exact or Over)- clusters prediction by the ML function used (HoeffdingTree, J48, RandomForest, RandomTree), the id of the function (2, 3, 4) and the number of clusters (64, 128, 256);
* `result_clusters_predictions.csv` : contains the result of clusters predictions, it shows the cluster of the testing set and the cluster predicted by the ML function used (HoeffdingTree, J48, RandomForest, RandomTree), the id of the function (2, 3, 4) and the number of clusters (64, 128, 256);
* `result_memory_predictions.csv` : contains the result of memories predictions, it shows the memories used in the testing set and the memories predicted by the ML function used (HoeffdingTree, J48, RandomForest, RandomTree), the id of the function (2, 3, 4) and the number of clusters (64, 128, 256);
* `result_cache_usage_predictions.csv` : contains the result of benefits of cache storage predictions, it shows the decision of cache storage choosen in the testing set and the one predicted by the ML function used (HoeffdingTree, J48, RandomForest, RandomTree) and the id of the function (2, 3, 4);

## usage

```java

    // Here we set needed parameters 

    int min = 0; // The minimum memory required in OWK in Bytes
    int max = 2000000000; // The maximum memory required in OWK in Bytes

    float rate = (float)0.5; // the Threshold for the cache storage

    String datafile = "fonction2.arff"; // source of data 

    int n = 128; // the number of clusters

    // Firstly, we create a new instance of ModelTrainer class
    ModelTrainer mt = new ModelTrainer(datafile, min, max, n, rate);

    mt.import_data(); // We import data from a .arff file and we format it 
    
    mt.split_dataset(); // We slpit the dataset into training and testing sets

    mt.build_and_save_model(); // We build the model based on J48 algorithm

    //Secondly, we create an instance of Predictor class 

    String modelfile = "model_memory_fonction2.model"; // model file of memory prediction
    Predictor p_memory = new Predictor(modelfile, min, max, n); // a preditor for the memory prediction

    float cluster = p_memory.predict_cluster(2506752); // Computing the cluster
    System.out.println("Cluster predicted : "+cluster); 

    System.out.println("Memory predicted : "+p_memory.predict_memory(cluster)); // Computing and printing the memory

    modelfile = "model_use_cache_fonction2.model"; // model file of cache storage prediction
    Predictor pcu = new Predictor(modelfile, min, max, n);// a predictor for the cache storage prediction
    System.out.print("Cache storage decision predicted : "+pcu.predict_cache_usage(2506752)); // Computing and printing the cache storage decision
    
```

It will print the following : 
```sh
    Cluster predicted : 4.0
    Memory predicted : 6.25E7
    Cache storage decision predicted : 1.0
```