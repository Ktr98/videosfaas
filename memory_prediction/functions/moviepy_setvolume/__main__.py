from moviepy.editor import *
import wget

def main(args):
    url = args['url']

    name_video = url.split("/")[-1]
    wget.download(url, name_video)
    
    clip = VideoFileClip(name_video)
    clip = clip.volumex(0.8)
    return {'outputsize':12}
