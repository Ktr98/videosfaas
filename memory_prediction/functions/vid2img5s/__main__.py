from moviepy.editor import *
import wget

def main(args):
    url = args['url']

    name_video = url.split("/")[-1]
    wget.download(url, name_video)
    
    clip = VideoFileClip(name_video)
    img = clip.to_ImageClip(t='00:00:05')

    return {'outputsize':12}
