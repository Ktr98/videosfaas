from moviepy.editor import *
import wget

def main(args):
    url = args['url']

    name_video = url.split("/")[-1]
    wget.download(url, name_video)
    
    clip = VideoFileClip(name_video)
    clip.resize(width=500)

    return {'outputsize':12}
