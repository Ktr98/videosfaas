# Structuration

Here is the folder taking care of machine learning process to predict memory used by functions in **Openwhisk** .

There is two folders here:

## Functions folder

Which consists of the codes of different functions (in python with **moviepy** library) took randomly between the most used of videos processing functions in openwhisk.

There is five folders (each function has his folder), and its code is set in a `__main__.py`:

* **moviepy_setvolume** : set the audio volume of a clip at 80%
* **moviepy_subclip** : extract a subclip related to the five first seconds of a clip
* **resize_clip** : as is name is clear, it consists of resize a clip
* **set_start_clip_at_5s** : the goal here is to change the starting time of the clip at the fift second
* **vid2img5s** : extracts the image at the fift second of the video

## ML folder

This folder is the central one of the project, because we have : 

* The **csv** folder which contains csvs datasets on functions mentionned earlier:
    * **fonction1.csv** : linked to **moviepy_setvolume**
    * **fonction2.csv** : linked to **moviepy_subclip**
    * **fonction3.csv** : linked to **resize_clip**
    * **fonction4.csv** : linked to **set_start_clip_at_5s**
    * **fonction5.csv** : linked to **vid2img5s**

* The **update_model.py** file : used to generate a prediction model based on 1-nn classifier. It works like this : 

    * Data are loaded from csv file
    * And then, a cluster column is added : based on 128 clusters of memories between 0 and 2Go, then, if for a specific execution of the function its memory is situated between a particular interval, we put this execution in the related cluster
    * After that, we split the datset in training and testing sets, considering **input feature** as the video size and the **output feature** as the cluster
    * Finally, we train the 1-nn model on training set and save it to `model_{function_name}.joblib` file.

* The **predict.py** file: used to predict the memory of a specific execution :
 
    * We predict the cluster of exections in the testing set and build accuracy of this prediction, regarding also its mean squared error 
    * And, the maximum memory of those in the cluster predicted is taken as the predicted memory of the execution carried on (The accuracy is also observed, here by dividing the number of memories predicted greater than those really used in the testing set executions, by the length of the testing set).

* The **result_video_csv.csv** file: used to show the perfomances of the algorithm on the testing set with different values of number of clusters (64, 128, 256).

* The **mlv_process.py** file: is a module consisting of one class allowing updating the model and prediction on a new instance of a video size.
