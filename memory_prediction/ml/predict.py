import sys
import joblib

def main(model, size):
    min_=0
    max_=20000000
    nb_clt=128
    
    # loading a model
    clt_mod = joblib.load(model)

    # predicting memory for the given size
    pred = clt_mod.predict([[int(size)]])

    # printing the memory predicted 
    print(min_+pred[0]*(max_-min_)/nb_clt)

main(sys.argv[1], sys.argv[2])
