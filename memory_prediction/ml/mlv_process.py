import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
import joblib

class VideoPrediction:
    
    # Constructor
    def __init__(self, input_data, output_model): 
        self.input_data = input_data
        self.output_model = output_model
        self.min = 0
        self.max = 2000000000
        self.nb_clt = 128
        self.model = KNeighborsClassifier(n_neighbors=5)
        
    # Whole part of a number
    def E(self, x) :
        if (x == int(x)) :
            return x
        elif (x>=0) :
            return int(x)
        else :
            return -self.E(-x)-1
    
    # Building and saving the model
    def build_model(self):

        # importing and transform data
        data = pd.read_csv(self.input_data, sep=";")
        X = data["image_size"]
        output="truth_memory"

        y= data[output]

        # building clusters of memories
        clusters = []
        for mem in y:
            nb = (mem-self.min)*self.nb_clt/(self.max-self.min)
            e_nb = self.E(nb)
            if(e_nb-nb == 0) :
                clusters.append(e_nb)
            else:
                clusters.append(e_nb+1)
        data["cluster"] = clusters

        y = data["cluster"]

        # spliting the dataset
        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.66, random_state=54)
        Xtrain = [[X_train[j]] for j in y_train.keys()]
        Xtest = [[X_test[j]] for j in y_test.keys()]
        ytrain = [y_train[j] for j in y_train.keys()]
        ytest = [y_test[j] for j in y_test.keys()]

        # create and train the model 1-nn classifier
        clf = self.model
        clt_mod = clf.fit(Xtrain, ytrain)

        # save the model
        joblib.dump(clt_mod, self.output_model)
    
    # Predicting for a new instance
    def prediction(self, size):

        # loading a model
        clt_mod = joblib.load(self.output_model)

        # predicting memory for the given size
        pred = clt_mod.predict([[int(size)]])

        # printing the memory predicted 
        return self.min+pred[0]*(self.max-self.min)/self.nb_clt
