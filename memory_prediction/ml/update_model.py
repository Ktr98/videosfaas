import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
import joblib
import sys

def E(x) :
    if (x == int(x)) :
        return x
    elif (x>=0) :
        return int(x)
    else :
        return -E(-x)-1

def main(input_data, output_model) :
	
    # importing and transform data
    data = pd.read_csv(input_data)
    X = data[data.keys()[0]]
    output=data.keys()[1]

    y= data[output]

    # number of clusters
    nb_clt = 128

    # respectively, minimum and maximum of the memories' set 
    min_ = 0
    max_ = 20000000

    # building clusters of memories
    clusters = []
    for mem in y:
        nb = (mem-min_)*nb_clt/(max_-min_)
        e_nb = E(nb)
        if(e_nb-nb == 0) :
            clusters.append(e_nb)
        else:
            clusters.append(e_nb+1)
    data["cluster"] = clusters

    y = data["cluster"]

    # spliting the dataset
    X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8, random_state=54)
    Xtrain = [[X_train[j]] for j in y_train.keys()]
    Xtest = [[X_test[j]] for j in y_test.keys()]
    ytrain = [y_train[j] for j in y_train.keys()]
    ytest = [y_test[j] for j in y_test.keys()]

    # create and train the model 1-nn classifier
    clf = KNeighborsClassifier(n_neighbors=1)
    clt_mod = clf.fit(Xtrain, ytrain)

    # save the model
    joblib.dump(clt_mod, output_model)

main(sys.argv[1], sys.argv[2])
